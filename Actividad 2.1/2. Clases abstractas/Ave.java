/**
 *
 * @author Jorge Alberto Luna
 */

public class Ave extends Animal{
	
	public Ave() {
		super();
		setNombre("Ave");
	}

	public void moverse() {
		System.out.println("El Ave esta volando.");
		
	}

}
