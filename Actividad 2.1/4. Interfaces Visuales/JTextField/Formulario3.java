import javax.swing.*;
import java.awt.event.*;
public class Formulario3 extends JFrame implements ActionListener {
    private JLabel label1,label2;
    private JTextField textfield1,textfield2;
    private JButton boton1;
    public Formulario3() {
        setLayout(null);
        label1=new JLabel("Usuario:");
        label1.setBounds(10,10,100,30);
        add(label1);
        label2=new JLabel("Clave:");
        label2.setBounds(10,50,100,30);
        add(label2);
        textfield1=new JTextField();
        textfield1.setBounds(130,10,100,30);
        add(textfield1);
        textfield2=new JTextField();
        textfield2.setBounds(130,50,100,30);
        add(textfield2);
        boton1=new JButton("Confirmar");
        boton1.setBounds(10,100,100,30);
        add(boton1);
        boton1.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==boton1) {
            String cad1=textfield1.getText();
            String cad2=textfield2.getText();
            if (cad1.equals("jorge_luna")==true && cad2.equals("123456")==true) {
                setTitle("Bienvenido Jorge Luna");
            } else {
                setTitle("Acceso denegado");
            }
        }
    }
    
    public static void main(String[] ar) {
        Formulario3 formulario1=new Formulario3();
        formulario1.setBounds(0,0,240,200);
        formulario1.setVisible(true);
    }    
}