import javax.swing.*;
public class Formulario2 extends JFrame {
    private JLabel label1,label2,label3;
    
    public Formulario2() {
        setLayout(null);
        label1=new JLabel("Jorge Luna");
        label1.setBounds(10,20,100,30);
        add(label1);
        label2=new JLabel("Edgar Lopez");
        label2.setBounds(10,60,100,30);
        add(label2);
        label3=new JLabel("Juan Soto");
        label3.setBounds(10,100,100,30);
        add(label3);
    }
    
    public static void main(String[] ar) {
        Formulario2 formulario1=new Formulario2();
        formulario1.setBounds(0,0,300,200);
        formulario1.setVisible(true);
    }
}