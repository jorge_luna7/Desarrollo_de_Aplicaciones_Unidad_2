import javax.swing.*;
import java.awt.event.*;
public class Formulario1 extends JFrame implements ActionListener {
    JButton boton1;
    public Formulario1() {
        setLayout(null);
        boton1=new JButton("Jorge haz click para finalizar");
        boton1.setBounds(300,250,100,30);
        add(boton1);
        boton1.addActionListener(this);
    }
    
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==boton1) {
            System.exit(0);
        }
    }
    
    public static void main(String[] ar) {
        Formulario1 formulario1=new Formulario1();
        formulario1.setBounds(0,0,450,350);
        formulario1.setVisible(true);
    }
}